Belt Splicing Alignment Tool
============================

When you need a closed loop belt of a length that you can't buy,
there have been several techniques recommended, including diagonal
cuts with pins through teeth, and cutting through the middle of two
teeth and sewing them together.

However, the strength of the belt is in the fiberglass or steel
cords running longitudinally, not in the teeth. These methods 
do not make a strong loop.

This tool allows you to make a well-aligned splice in many
timing belts that preserves at least much of the strength.  It
is unlikely to be as precise as a commercially-prepared closed
loop, but may be good enough for many uses.


## Ingredients

* More belt than you need for the loop
* Some sort ot flexible adhesive:
   * Rubber-bearing CA glue (I used [IC-2000](https://www.amazon.com/gp/product/B002N507I0); perhaps [Loctite 1363589](https://www.amazon.com/Loctite-1363589-4-Gram-Control-Adhesive/dp/B01HPT0AWQ) or [Starbond KBL-500](https://www.amazon.com/Starbond-KBL-500-Flexible-Premium-Toughened/dp/B00BUVAVC0) would work as well)
   * Neoprene adhesive (e.g. [Black Witch Neoprene Adhesive](https://www.amazon.com/McNett-Black-Neoprene-Adhesive-Other/dp/B01LFLBKEM)) should work as well or better but I haven't tested it. On reprap forums, [leadinglights reported it as very strong for back-to-back bonding.](https://reprap.org/forum/read.php?1,670647,871716#msg-871716)
   * Hexane-based inner tube patch compound (untested at this time)
* A piece of cling wrap
* This jig, parameterized and printed for the belt you are joining
* Two or three machine screws (the default is M5) at least 20mm long
* Two or three hex nuts in the same size (the default is 8mm hex nuts for M5)
* A pin as the hinge pin for the jig (default is 6.35mm / ¼")

## Examples

* BeltSplicer is for HTD 5M belt 15mm wide using a ¼" hinge pin.
* BeltSplicer2GT-6 is for 2GT/GT2 6mm belt, the type typically used for 3D printers, and using a strand of 1.75mm printer filament as a hinge pin.

## Steps

1. Parameterize the FreeCAD model or choose the model for your belt, hinge pin, screws, and nuts. Pay particular attention to:
   1. Width of the belt
   1. Pitch of the teeth
   1. Number of teeth to overlap (I suggest 15cm worth of teeth or more)
   1. Extra teeth visible in the center of the jig when the two side blocks are clamped in place.
   1. Sizes of holes for clearance and size of nut if you substitute something else for M5.
1. Print the jig. The type of plastic won't matter unless you glue the bottom belt in place.
1. Prepare the jig for use:
   1. Use a screw to pull a nut into each nut socket on the bottom of the main block.
   1. Use the hinge pin to connect the three smaller blocks to the larger block so that they can rotate over the top. Make sure they rotate smoothly. Sand the edges of the hinges if necessary to enable smooth movement.
1. Arrange for a piece of belt to align the cut teeth in the jig:
   1. Cut a sacrificial piece of belt as long as the groove in the jig to glue into the groove
   1. Use another piece temporarily
   1. If you aren't making the loop _in situ,_ you may use the opposite side of the same belt
1. Cut the desired length of belt for the loop, plus the chosen number of teeth of overlap.
1. At end **A**, use a fresh blade (the typical x-acto #11 is a good choice) to cut away the designated number of teeth. Start by cutting off most of the tooth, then carefully slice off layers of neoprene until you feel and hear the blade touch the cord. You should see the cord on the cut surface. You may find that a chisel is useful.
1. At end **B**, mark the _back_ side of the belt to the same length as the teeth you have removed from end A. This should start opposite the gap between two teeth. Cut away the back of the belt to, but not through, the cord.
1. Test fit the ends together, with the cut faces touching each other, and with the teeth interlocked with another section of the belt (or the sacrificial piece). If they don't fit well, you may have to trim the back of end **B** slightly further until they fit well. Your goal is a tight fit at both cut ends.
1. Use a coarse sandpaper (60 grit or so) block on the cut surfaces until the belts show clearly.
1. Trim the ends (again), if necessary, to ensure a precise fit, without leaving substantial gaps.
1. Place the alignment piece of belt in the gap in the jig, teeth up.
   1. If you are using a sacrificial piece, you may glue it in place. Set one piece of cling wrap at least as wide as the jig over the alignment belt teeth in the gap, loosely.
   1. If you are using a temporary piece of belt, wrap a single layer of cling wrap around the belt.
1. Cut holes in the cling wrap for the screws so that the cling wrap does not interfere with the screws.
1. Make sure that there are no unintended twists in the belt, and lay ends **A** and **B** on top of the cling wrap, teeth down and cut sections overlapped, with the cling wrap between the belt to be joined and the aligning section in the groove.
1. Screw down the blocks at the edges to hold the belts together in place, making sure that the section to be joined in the middle is fully exposed.
1. Lift the loose end **A** and put a thin coat of adhesive on end **B**.
1. Lower end **A** carefully, making sure it is aligned, and immediately rotate the middle block to hold it in place.
   1. If using CA glue, hold firmly in place for as long as the instructions say. I recommend against using an accelerator unless the manufacturer says it will not reduce flexibility.
   1. At least if using a slower-setting glue, use a third screw to hold the middle block in place while it sets.
1. After it is set, possibly after it has cured, remove it from the jig.
1. Let cure fully before putting significant tension on the belt. (For example, IC-2000 [reaches maximum strength in three hours](https://bsi-inc.com/hardware/ic_2000.html))

The glued section will (at least with CA glue) be stiffer than the rest, but in my experience it runs fine through toothed pulleys.

Credits: I'm sure I got pieces of this from various places on the internet, but I can't find this particular process. I think this is a mashup of the glue advice in back-to-back bonding ideas from https://reprap.org/forum/read.php?1,670647 and some pointer to IC-2000 I found somewhere, combined with the now-anonymous (deleted account) [PSA: how to splice belts for custom length closed loops](https://www.reddit.com/r/3Dprinting/comments/8r2tvf/psa_how_to_splice_belts_for_custom_length_closed/) that used _hot glue._
